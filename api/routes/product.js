const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Product = require('../../models/products');
// const path = require('path');
const multer = require('multer');

// Multer setup
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  // Reject file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  fileFilter: fileFilter
});

// routes
router.get('/', (req, res, next) => {
  Product.find()
    .select('name _id productImage')
    .then(result => {
      const response = {
        count: result.length,
        products: result.map(result => {
          return {
            _id: result._id,
            name: result.name,
            productImage: result.productImage,
            url: {
              url: 'http://localhost:8080/images/' + result._id
            }
          };
        })
      };
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        message: err
      });
    });
});

router.post('/', upload.single('productImage'), (req, res, next) => {
  console.log(req.file);
  const product = new Product({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    productImage: req.file.path
  });
  product
    .save()
    .then(result => {
      console.log(result);
      res.status(200).json({
        product: {
          name: result.name,
          _id: result._id,
          productImage: result.productImage
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        message: 'Post request error',
        error: err
      });
    });
});
router.get('/:id', (req, res, next) => {
  const id = req.params.id;
  Product.findById(id)
    .select('name _id productImage')
    .then(result => {
      console.log(result);
      if (result) {
        res.status(200).json({
          product: result
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.delete('/:id', (req, res, next) => {
  const id = req.params.id;
  Product.deleteOne({ _id: id })
    .then(result => {
      if (!result) {
        res
          .status(500)
          .json({ success: false, result: 'No user found with that id' });
      } else {
        res.status(200).json({ success: true, result: result });
      }
    })
    .catch(err => {
      res.json({ success: false, result: err });
    });
});
module.exports = router;
