const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const multer = require('multer');
const productRoutes = require('./api/routes/product');

// DATABASE
mongoose
  .connect('mongodb://localhost/images', { useNewUrlParser: true })
  .then(() => console.log('MongoDB connected'))
  .catch(err => {
    console.log(err);
  });

// middleware
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/images/', productRoutes);

const port = 8080;
app.listen(port, () => console.log('listening on port 8080'));
